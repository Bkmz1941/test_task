<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Front'], function () {
    Route::get('/', 'MainController@index')->name('index');
});
Route::group(['namespace' => 'Admin'], function () {
    Route::get('/admin', 'AdminController@index')->name('admin');
    Route::post('/event', 'AdminController@event')->name('event');
    Route::post('/event_open', 'AdminController@event_open')->name('event_open');
    Route::post('/event_close', 'AdminController@event_close')->name('event_close');
});
Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/home', 'HomeController@index');
