<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css'>
    <link href="front_assets/common/img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="front_assets/common/img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="front_assets/common/img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="front_assets/common/img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
    <link href="front_assets/common/img/favicon.png" rel="icon" type="image/png">
    <!-- HTML5 shim and Respond.js for < IE9 support of HTML5 elements and media queries -->
    <!--[if lt IE 9] -->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <!--[endif]-->
    <!-- Vendors Styles -->
    <!-- v1.0.0 -->
    <link rel="stylesheet" type="text/css" href="front_assets/vendors/bootstrap/dist/css/bootstrap.min.css">
    <!-- Clean UI Admin Template Styles -->
    <link rel="stylesheet" type="text/css" href="front_assets/common/css/source/main.css">
    <!-- Vendors Scripts -->
    <!-- v1.0.0 -->
    <script src="front_assets/vendors/jquery/dist/jquery.min.js"></script>
    <script src="/admin_assets2/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.js"></script>
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
<div class="cwt__block cwt__header-fixed">
    <div class="container">
        <div class="cwt__header-fixed__container">
            <div class="row">
                <div class="col-xs-9">
                    <nav class="nav nav-inline pull-xs-left cwt__main-menu cwt__main-menu--dark">
                        <a href="/" class="nav-link btn btn-primary cwt__main-menu__link cwt__main-menu__link--button">На главную</a>
                    </nav>
                </div>
                <div class="col-xs-3">
                    <div class="cwt__logo cwt__logo--small" style="width:12rem;">
                        <a href="index.html">
                            <img src="front_assets/common/img/logo-inverse.png" alt="Clean UI Admin Template + Free Ultimate Premium Bundle + Landing Pages + BEM + Angular2 Starter Kit" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

        @yield('content')

    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>
