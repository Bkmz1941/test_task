@extends('layout.app_front')

@section('content')
    <div class="cwt__block cwt__header-fixed">
        <div class="container">
            <div class="cwt__header-fixed__container">
                <div class="row">
                    <div class="col-xs-9">
                        <nav class="nav nav-inline pull-xs-left cwt__main-menu cwt__main-menu--dark">
                            <a href="/login" class="nav-link btn btn-primary cwt__main-menu__link cwt__main-menu__link--button">Админка</a>
                        </nav>
                    </div>
                    <div class="col-xs-3">
                        <div class="cwt__logo cwt__logo--small">
                            <a href="index.html">
                                <img src="front_assets/common/img/logo-inverse.png" alt="Clean UI Admin Template + Free Ultimate Premium Bundle + Landing Pages + BEM + Angular2 Starter Kit" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Preview Block -->
    <div class="cwt__block cwt__preview">
        <div class="cwt__preview__container">
            <div class="cwt__preview__title">
                Главная страница
            </div>
            <div id="content"></div>
        </div>
    </div>

    <script>
        event_open();

        function event_open() {
            var token = window.Laravel.csrfToken;
            var url = window.location.origin + '/event_open';
            $.post(url, {
                _token: token
            }, function (response) {
                $.each(response.data, function (index, value){
                    console.log(value);
                    $.notify({
                        title: '<strong>'+value.name+'</strong>'+'<br>',
                        message: value.text+'<br>'+value.user,
                        url: value.link
                    },{
                        type: 'info'
                    });
                });
            });
        }


        $(function(){

            // Sweet Alert
            $('.swal-btn-basic').click(function(e){
                e.preventDefault();
                swal("Here's a message!");
            });

            $('.swal-btn-text').click(function(e){
                e.preventDefault();
                swal({
                    title: "Here's a message!",
                    text: "It's pretty, isn't it?"
                });
            });

            $('.swal-btn-success').click(function(e){
                e.preventDefault();
                swal({
                    title: "Good job!",
                    text: "You clicked the button!",
                    type: "success",
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Success"
                });
            });

            $('.swal-btn-warning').click(function(e){
                e.preventDefault();
                swal({
                        title: "Are you sure?",
                        text: "Your will not be able to recover this file!",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonClass: "btn-default",
                        confirmButtonClass: "btn-warning",
                        confirmButtonText: "Remove",
                        closeOnConfirm: false
                    },
                    function(){
                        swal({
                            title: "Deleted!",
                            text: "File has been deleted",
                            type: "success",
                            confirmButtonClass: "btn-success"
                        });
                    });
            });

            $('.swal-btn-cancel').click(function(e){
                e.preventDefault();
                swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this imaginary file!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, remove it",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            swal({
                                title: "Deleted!",
                                text: "Your imaginary file has been deleted.",
                                type: "success",
                                confirmButtonClass: "btn-success"
                            });
                        } else {
                            swal({
                                title: "Cancelled",
                                text: "Your imaginary file is safe :)",
                                type: "error",
                                confirmButtonClass: "btn-danger"
                            });
                        }
                    });
            });

            $('.swal-btn-custom-img').click(function(e){
                e.preventDefault();
                swal({
                    title: "Sweet!",
                    text: "Here's a custom image.",
                    confirmButtonClass: "btn-success",
                    imageUrl: '../../modules/dummy-assets/common/img/avatars/1.jpg'
                });
            });

            $('.swal-btn-info').click(function(e){
                e.preventDefault();
                swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "info",
                    showCancelButton: true,
                    cancelButtonClass: "btn-default",
                    confirmButtonText: "Info",
                    confirmButtonClass: "btn-primary"
                });
            });


            // Bootstrap Notify
            $('#notify-basic').on('click', function() {
                $.notify("Hello World");
            });

            $('#notify-passing-title').on('click', function() {
                $.notify({
                    title: "Welcome:",
                    message: "This plugin has been provided to you by Robert McIntosh aka mouse0270"
                });
            });

            $('#notify-passing-html').on('click', function() {
                $.notify({
                    title: "<strong>Welcome:</strong> ",
                    message: "This plugin has been provided to you by Robert McIntosh aka <a href=\"https://twitter.com/Mouse0270\" target=\"_blank\">@mouse0270</a>"
                });
            });

            $('#notify-url').on('click', function() {
                $.notify({
                    message: "Check out my twitter account by clicking on this notification!",
                    url: "https://twitter.com/Mouse0270"
                });
            });

            $('#notify-icomoon-premuim').on('click', function() {
                $.notify({
                    icon: 'icmn-database',
                    message: "Everyone loves font icons! Use them in your notification!"
                });
            });

            $('#notify-fontawesome').on('click', function() {
                $.notify({
                    icon: 'fa fa-star',
                    title: 'Title',
                    message: "Everyone loves font icons! Use them in your notification!"
                });
            });

            $('#notify-center').on('click', function() {
                $.notify({
                    icon: 'font-icon font-icon-warning',
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    placement: {
                        align: "center"
                    }
                });
            });

            $('#notify-bottom').on('click', function() {
                $.notify({
                    icon: 'font-icon font-icon-warning',
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    placement: {
                        from: "bottom"
                    }
                });
            });

            $('#notify-bottom-center').on('click', function() {
                $.notify({
                    icon: 'font-icon font-icon-warning',
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    placement: {
                        from: "bottom",
                        align: "center"
                    }
                });
            });

            $('#notify-default').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'default'
                });
            });

            $('#notify-primary').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'primary'
                });
            });

            $('#notify-secondary').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'secondary'
                });
            });

            $('#notify-success').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'success'
                });
            });

            $('#notify-warning').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'warning'
                });
            });

            $('#notify-danger').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'danger'
                });
            });

            $('#notify-info').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'info'
                });
            });

        });
    </script>


@endsection