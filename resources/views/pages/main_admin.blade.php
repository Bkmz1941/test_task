@extends('layout.app_admin')

@section('content')
    <div class="cat__top-bar">
        <!-- left aligned items -->
        <div class="cat__top-bar__left">

        </div>
        <!-- right aligned items -->
        <div class="cat__top-bar__right">
            <a href="{{ url('/logout') }}" class="nav-link btn btn-danger cwt__main-menu__link cwt__main-menu__link--button" style="margin-right: 10px;">Выход</a>
            <a href="/" class="nav-link btn btn-primary cwt__main-menu__link cwt__main-menu__link--button">Назад</a>
        </div>
    </div>
    <div class="cat__content">
        <section class="card">
            <div class="card-header">
                <h2>Создать уведомление</h2>
                <div class="form-group row">
                    <label class="col-md-5 col-form-label" for="l0">Название уведомления</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" placeholder="Новый урок" id="name">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-5 col-form-label" for="l0">Текст уведомления (до 280 символов)</label>
                    <div class="col-md-7">
                        <input class="form-control" rows="3" maxlength="280" placeholder="Правовые взаимоотношения к курсу Авторское право" id="text">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-5 col-form-label" for="l0">Ссылка на которую ведет клик по уведомлению</label>

                    <div class="col-md-7">
                        <input type="text" class="form-control" placeholder="academy.pravo.ru" id="link">
                        <p style="color: grey; margin-bottom: 0px;">* В формате: https://framework7.io/</p>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-5 col-form-label" for="l0">Автор уведомления</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" placeholder="Служба поддержки" id="user">
                    </div>
                </div>
                <button type="button" class="btn btn-outline-primary mr-2 mb-2" onclick="event_click()">Добавить уведомление</button>
            </div>
        </section>
    </div>

    <script>
        function event_click() {
            var token = window.Laravel.csrfToken;
            var url = window.location.origin + '/event';
            var name = $('#name').val();
            var text = $('#text').val();
            var link = $('#link').val();
            var user = $('#user').val();
            $.post(url, {
                _token: token,
                text: text,
                name: name,
                link: link,
                user: user
            }, function (response) {
                if (response.status == true) {
                    $('#name').val('');
                    $('#text').val('');
                    $('#link').val('');
                    $('#user').val('');
                    $.notify({
                        title: 'Успешно'+'<br>',
                        message: 'Уведомление создано'
                    },{
                        type: 'success'
                    });
                } else {
                    $.notify({
                        title: 'Ошибка'+'<br>',
                        message: response.message
                    },{
                        type: 'danger'
                    });
                }
            });
        }





        $(function(){

            // Sweet Alert
            $('.swal-btn-basic').click(function(e){
                e.preventDefault();
                swal("Here's a message!");
            });

            $('.swal-btn-text').click(function(e){
                e.preventDefault();
                swal({
                    title: "Here's a message!",
                    text: "It's pretty, isn't it?"
                });
            });

            $('.swal-btn-success').click(function(e){
                e.preventDefault();
                swal({
                    title: "Good job!",
                    text: "You clicked the button!",
                    type: "success",
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Success"
                });
            });

            $('.swal-btn-warning').click(function(e){
                e.preventDefault();
                swal({
                        title: "Are you sure?",
                        text: "Your will not be able to recover this file!",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonClass: "btn-default",
                        confirmButtonClass: "btn-warning",
                        confirmButtonText: "Remove",
                        closeOnConfirm: false
                    },
                    function(){
                        swal({
                            title: "Deleted!",
                            text: "File has been deleted",
                            type: "success",
                            confirmButtonClass: "btn-success"
                        });
                    });
            });

            $('.swal-btn-cancel').click(function(e){
                e.preventDefault();
                swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this imaginary file!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, remove it",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            swal({
                                title: "Deleted!",
                                text: "Your imaginary file has been deleted.",
                                type: "success",
                                confirmButtonClass: "btn-success"
                            });
                        } else {
                            swal({
                                title: "Cancelled",
                                text: "Your imaginary file is safe :)",
                                type: "error",
                                confirmButtonClass: "btn-danger"
                            });
                        }
                    });
            });

            $('.swal-btn-custom-img').click(function(e){
                e.preventDefault();
                swal({
                    title: "Sweet!",
                    text: "Here's a custom image.",
                    confirmButtonClass: "btn-success",
                    imageUrl: '../../modules/dummy-assets/common/img/avatars/1.jpg'
                });
            });

            $('.swal-btn-info').click(function(e){
                e.preventDefault();
                swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "info",
                    showCancelButton: true,
                    cancelButtonClass: "btn-default",
                    confirmButtonText: "Info",
                    confirmButtonClass: "btn-primary"
                });
            });


            // Bootstrap Notify
            $('#notify-basic').on('click', function() {
                $.notify("Hello World");
            });

            $('#notify-passing-title').on('click', function() {
                $.notify({
                    title: "Welcome:",
                    message: "This plugin has been provided to you by Robert McIntosh aka mouse0270"
                });
            });

            $('#notify-passing-html').on('click', function() {
                $.notify({
                    title: "<strong>Welcome:</strong> ",
                    message: "This plugin has been provided to you by Robert McIntosh aka <a href=\"https://twitter.com/Mouse0270\" target=\"_blank\">@mouse0270</a>"
                });
            });

            $('#notify-url').on('click', function() {
                $.notify({
                    message: "Check out my twitter account by clicking on this notification!",
                    url: "https://twitter.com/Mouse0270"
                });
            });

            $('#notify-icomoon-premuim').on('click', function() {
                $.notify({
                    icon: 'icmn-database',
                    message: "Everyone loves font icons! Use them in your notification!"
                });
            });

            $('#notify-fontawesome').on('click', function() {
                $.notify({
                    icon: 'fa fa-star',
                    title: 'Title',
                    message: "Everyone loves font icons! Use them in your notification!"
                });
            });

            $('#notify-center').on('click', function() {
                $.notify({
                    icon: 'font-icon font-icon-warning',
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    placement: {
                        align: "center"
                    }
                });
            });

            $('#notify-bottom').on('click', function() {
                $.notify({
                    icon: 'font-icon font-icon-warning',
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    placement: {
                        from: "bottom"
                    }
                });
            });

            $('#notify-bottom-center').on('click', function() {
                $.notify({
                    icon: 'font-icon font-icon-warning',
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    placement: {
                        from: "bottom",
                        align: "center"
                    }
                });
            });

            $('#notify-default').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'default'
                });
            });

            $('#notify-primary').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'primary'
                });
            });

            $('#notify-secondary').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'secondary'
                });
            });

            $('#notify-success').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'success'
                });
            });

            $('#notify-warning').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'warning'
                });
            });

            $('#notify-danger').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'danger'
                });
            });

            $('#notify-info').on('click', function() {
                $.notify({
                    title: '<strong>Heads up!</strong>',
                    message: 'You can use any of bootstraps other alert styles as well by default.'
                },{
                    type: 'info'
                });
            });

        });
    </script>
@endsection