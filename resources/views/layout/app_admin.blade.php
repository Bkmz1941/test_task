<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
        <link rel="stylesheet" type="text/css" href="/admin_assets2/vendors/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets2/vendors/jscrollpane/style/jquery.jscrollpane.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets2/modules/core/common/core.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets2/modules/vendors/common/vendors.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets2/modules/menu-left/common/menu-left.cleanui.css">
        <link rel="stylesheet" type="text/css" href="/admin_assets2/modules/top-bar/common/top-bar.cleanui.css">
        <script src="front_assets/vendors/jquery/dist/jquery.min.js"></script>
        <script src="/admin_assets2/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.js"></script>
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>

</head>
<body>
@yield('content')
</body>
</html>