<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use App\Model\Events;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\AjaxRequest;

class AdminController extends Controller
{
    public function index() {
        return view('pages.main_admin');
    }

    public function event(AjaxRequest $request) {
        $name = $request->input('name', false);
        $text = $request->input('text', false);
        $user = $request->input('user', false);
        $link = $request->input('link', false);
        $status = false;

        DB::beginTransaction();
        try {
            Events::create([
                'name' => $name,
                'text' => $text,
                'user' => $user,
                'link' => $link,
                'status' => '1'
                ]);
            $status = true;
            DB::commit();
            return response()->json(['status' => $status]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['status' => $status]);
        }
    }
    public function event_open(Request $request) {
        $status = true;
        $event = Events::where('status','1')->get();
        $data = [];
        foreach ($event as $value) {
            array_push($data, [
                'name' => $value->name,
                'text' => $value->text,
                'link' => $value->link,
                'user' => $value->user
            ]);
        }
        return response()->json(['status' => $status, 'data' => $data]);
    }

    public function event_close(Request $request) {
        $status = false;
        $href = $request->input('href', false);
        $event = Events::where('link', $href)->first();
        if (isset($event->id)) {
            $event = Events::where('link', $href)->delete();
            $status = true;
            return response()->json(['status' => $status]);
        } else {
            return response()->json(['status' => $status]);
        }
    }
}
